DETAILS
```
#### GOALS  :   ###########################################################
   To create utilities function to pre-process, re-format time series data.
   Time series data : sinus data

   Initially 1D (one time serie)  or 
             2D (time series stored as image)  or .npz files


   models are either in TFlow or Pytorch and designed for 2D tensors.

   However, we need to change the local loader to accept multidimenstionnal tensor
   for each of the models loader.  


   1) Data Generation  and Data Transformation into npz files or raw images.

   2) Change the local model loader : to accept our format or create a wrapper.

   3) Basic run with models (accuracy is NOT impt, just workflow)




#### Folder structure   ####################################################
   env_tf.yml   : environnment
   models/   : Contains all the models VAE and GAN on images.
   adata/    : Contains all the DATA BUT  not loaded with repo... BE CAREFUL.

util.py contains ALL the utilties functions
cli_generate_data.py :  generator of data




#### Test models   ########################################################
  models/beta_VAE/  is our 1st model.
     run_dsprites_B_gamma100_z10.sh  is working well on pytorch


  Testing code is run_sinus.txt

  models/Beta_VAE/dataset.py  : contains THE LOCAL CODE to handle our data.

   



```


