
import os, sys
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import pandas as pd





def generate_random_sin(n_rand_starts = 100, amplitude = 1, n_pis = 4, omega = 1, step = 0.2):
  r = np.random.randint(n_rand_starts)

  x = np.arange(r, r + n_pis*np.pi, step)   # start,stop,step
  y = amplitude * np.sin(omega * x)

  return x,y



def generate_train(N_type=100, amax=5, wmax=10.0 ) :
	#N_type = 100
	X = []

	for type_i in range(N_type):
	  for amp_i in range(1, amax): # different amplitude
	    for omega_i in range(1, wmax, 0.5): # different omega
	      X.append(generate_random_sin(N_type, amp_i, 4, omega_i, 0.1))
    
    



def plot_save_disk(x,y, filename, xmax=64, ymax=64):
	#x =  np.arange(0,10,0.1)
	# y = np.cos(x)
	fig = plt.figure(frameon=False, figsize=(xmax, ymax), dpi=1)

	ax = plt.Axes(fig, [0., 0., 1., 1.])
	ax.set_axis_off()
	fig.add_axes(ax)
	plt.plot(x,y, c="black", linewidth=100 )
	fig.savefig(filename)



def generate_train_img(folder, N_type=100, amax=5, wmin=5, wmax=10.0, step = 0.1, wfreq=0.5 ) :
    #N_type = 100
    X = []
    os.makedirs(folder, exist_ok=True)
    folder = os.path.abspath(folder)

    for type_i in range(N_type):
      for amp_i in range(1, amax): # different amplitude
        for omega_i in range(wmin, wmax, 1): # different omega
          omega_ii = wfreq * omega_i
                
          x,y = generate_random_sin(N_type, amp_i, 4, omega_ii, step)
          filename = f'{folder}/sin_{amp_i}-{omega_ii}-{type_i}'.replace(".","_")
          filename = filename + ".png"

          plot_save_disk(x,y, filename, xmax=64, ymax=64)







print("loaded")

