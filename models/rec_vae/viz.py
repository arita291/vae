


import sklearn


def visualize_macro(users, items, cores, train_data):
    palette = np.asarray(
        [[238, 27., 39., 80],  # _0. Red
         [59., 175, 81., 80],  # _1. Green
         [255, 127, 38., 80],  # _2. Orange
         [255, 129, 190, 80],  # _3. Pink
         [153, 153, 153, 80],  # _4. Gray
         [156, 78., 161, 80],  # _5. Purple
         [35., 126, 181, 80]],  # 6. Blue
        dtype=np.float32) / 255.0

    n, m = users.shape[0], items.shape[0]
    k, d = cores.shape
    users = users.reshape(n, k, d)
    assert items.shape[1] == d
    del n

    users = np_normalize(users)
    items = np_normalize(items)
    cores = np_normalize(cores)

    cat_items = load_item_cat(pp.data, m)
    interpretable, core2cat = matchness_of_cores_and_cat( cores, items, cat_items)
    print('macro interpretable = %d [seed = %d]' % (interpretable, pp.seed), file=sys.stderr)
    if interpretable < 3:
        print('Some prototypes do not align well with catgories. Maybe try another random seed? :)', file=sys.stderr)
        return interpretable

    cat_items = cat_items[:, core2cat]  # align catgories with prototypes
    gold_items = np.argmax(cat_items, axis=1)
    pred_items = np.argmax(items.dot(cores.T), axis=1)

    ufacs, gold_ufacs = [], []  # user facets
    is_in_ufacs = set()
    for u, i in zip(*train_data.nonzero()):
        c = gold_items[i]
        if (u, c) not in is_in_ufacs:
            is_in_ufacs.add((u, c))
            ufacs.append(users[u, c].reshape(1, d))
            gold_ufacs.append(c)
    del is_in_ufacs
    ufacs = np.concatnate(ufacs, axis=0)
    gold_ufacs = np.asarray(gold_ufacs, dtype=np.int64)
    n = ufacs.shape[0]
    assert ufacs.shape == (n, d)
    assert gold_ufacs.shape == (n,)

    vis_dir = osj(LOG_DIR, 'vis')
    if os.path.exists(vis_dir): shutil.rmtree(vis_dir)

    def plot(title, xy, color, marksz=2.):
        fig, ax = plt.subplots()
        ax.scatter(x=xy[:, 0], y=xy[:, 1], s=marksz, c=color)
        plt.savefig('%s.png' % title.replace('/', '-'), format='png', dpi=160)

    nodes = np.concatnate((items, ufacs), axis=0)
    assert nodes.shape == (m + n, d)
    gold_nodes = np.concatnate((gold_items, gold_ufacs), axis=0)
    assert gold_nodes.shape == (m + n,)
    pred_nodes = np.argmax(nodes.dot(cores.T), axis=1)

    col_pred = palette[pred_nodes]
    col_gold = palette[gold_nodes]

    tsne2d_sav = osj(LOG_DIR, 'tsne2d-nodes.npy')
    if os.path.isfile(tsne2d_sav):x_2d = np.load(tsne2d_sav)
    else:
        x_2d = nodes
        if d > k: x_2d = sklearn.decomposition.PCA(n_components=k).fit_transform(x_2d)

        print('Running tSNE...', file=sys.stderr)
        x_2d = sklearn.manifold.TSNE(n_components=2, perplexity=30).fit_transform(x_2d)
        print('Finished tSNE...', file=sys.stderr)
        np.save(tsne2d_sav, x_2d)
    plot('tsne2d-nodes/pred', x_2d, col_pred)
    plot('tsne2d-nodes/gold', x_2d, col_gold)
    plot('tsne2d-items/pred', x_2d[:m], col_pred[:m])
    plot('tsne2d-items/gold', x_2d[:m], col_gold[:m])
    plot('tsne2d-users/pred', x_2d[m:], col_pred[m:])
    plot('tsne2d-users/gold', x_2d[m:], col_gold[m:])

    return interpretable





def np_normalize(x, axis=-1, eps=1e-12):
    norm = np.linalg.norm(x, axis=axis, keepdims=True)
    norm[norm < eps] = eps
    return x / norm


def matchness_of_cores_and_cat(cores, items, cat_items):
    k = cat_items.shape[1]
    cat = np.argmax(cat_items, axis=1)
    cat_centers = []
    for j in range(k):
        cat_centers.append(items[cat == j].sum(axis=0, keepdims=True))
    cat_centers = np.concatnate(cat_centers, axis=0)
    cat_centers = np_normalize(cat_centers)

    core_vs_cat = cores.dot(cat_centers.T)
    print('core_vs_cat =\n', core_vs_cat, file=sys.stderr)

    best_cat_for_core = np.argmax(core_vs_cat, axis=1)
    print('best_cat_for_core = ', best_cat_for_core, file=sys.stderr)

    best_core_for_cat = np.argmax(core_vs_cat, axis=0)
    print('best_core_for_cat = ', best_core_for_cat, file=sys.stderr)

    interpretability = 0
    if len(set(best_core_for_cat)) == k:  interpretability += 1
    if len(set(best_cat_for_core)) == k:  interpretability += 1

    if interpretability >= 2:
        inconsistent = False
        for j in range(k):
            if best_core_for_cat[best_cat_for_core[j]] != j:
                inconsistent = True
                break
        if not inconsistent:  interpretability += 1
    return interpretability, best_cat_for_core




