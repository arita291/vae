"""
DONE :




TODO :






"""

from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import csv
import os
import shutil
import sys
import time

from os.path import join as osj

import pytz
import bottleneck as bn

import numpy as np
import pandas as pd
import sklearn.decomposition
import sklearn.manifold
import sklearn.preprocessing
from scipy import sparse

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torch.distributions.relaxed_categorical import RelaxedOneHotCategorical

from data_loading import *
from metrics import *

##############################################################################################################
##############################################################################################################
p = argparse.ArgumentParser()
p.add_argument('--data',   type=str, default="./data/alishop-7c" , help='./data/ml-latest-small, ./data/ml-1m, ''./data/ml-20m, or ./data/alishop-7c')
p.add_argument('--mode',   type=str, default='trn', help='trn/tst/vis, for training/testing/visualizing.')
p.add_argument('--logdir', type=str, default='./runs/')
p.add_argument('--seed',   type=int, default=98765, help='Random seed. Ignored if < 0.')
p.add_argument('--epoch',  type=int, default=500, help='Number of training epochs.')
p.add_argument('--batch',  type=int, default=500, help='Training batch size.')
p.add_argument('--lr',     type=float, default=1e-3, help='Initial learning rate.')
p.add_argument('--rg',     type=float, default=0.0, help='L2 regularization.')
p.add_argument('--keep',   type=float, default=0.5, help='Keep probability for dropout, in (0,1].')
p.add_argument('--beta',   type=float, default=0.2, help='Strength of disentanglement, in (0,oo).')
p.add_argument('--tau',    type=float, default=0.1, help='Temperature of sigmoid/softmax, in (0,oo).')
p.add_argument('--std',    type=float, default=0.075, help='Standard deviation of the Gaussian prior.')
p.add_argument('--gpu',    type=bool, default=False, help='Use GPU for training.')

#### Macro  part
p.add_argument('--kfac',   type=int, default=7, help='Number of facets (macro concepts).')
p.add_argument('--dfac',   type=int, default=100, help='Dimension of each facet.')
p.add_argument('--nogb',   action='store_true', default=False, help='Disable Gumbel-Softmax sampling.')


pp = p.parse_args()

pp.seed = int(time.time()) if pp.seed < 0 else pp.seed
LOG_DIR = '%s-%dT-%dB-%glr-%grg-%gkp-%gb-%gt-%gs-%dk-%dd-%d' % (
    pp.data.replace('/', '_'), pp.epoch, pp.batch, pp.lr, pp.rg, pp.keep,pp.beta, pp.tau, pp.std, pp.kfac, pp.dfac, pp.seed)
LOG_DIR += '-nogb' if pp.nogb else ""
LOG_DIR  = osj(pp.logdir, LOG_DIR)
batch_size_vad = batch_size_test = pp.batch

device = "cuda" if pp.gpu else "cpu"

# Function for random seed initialization
def set_rng_seed(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)

##############################################################################################################
class MyVae(nn.Module):
	"""docstring for MyVae"""
	def __init__(self, num_items):
		super(MyVae, self).__init__()
		# initialization of variables
		# They are pretty much same as that of the tf equavalent
		# In modst line, the corresponding tf code is commmented
		self.n_items = num_items
		kfac, dfac   = pp.kfac, pp.dfac
		self.lam     = pp.rg
		self.random_seed = pp.seed

		# The first fc layer of the encoder Q is the context embedding table.  ###############################
		self.q_dims = [num_items, dfac, dfac]
		self.weights_q, self.biases_q = [], []
		for i, (d_in, d_out) in enumerate( zip(self.q_dims[:-1], self.q_dims[1:])):
			if i == len(self.q_dims[:-1]) - 1: d_out *= 2  # mu & var

			weight_key = f"weight_q_{i}to{i+1}"
            # self.weights_q.append(tf.get_variable(name=weight_key, shape=[d_in, d_out],
            #                       initializer=tf.contrib.layers.xavier_initializer(seed=self.random_seed)))
			w = torch.empty(d_in, d_out, dtype=torch.float32, requires_grad=True, device=device)
			nn.init.xavier_uniform_(w)
			self.weights_q.append(w)
			# print(w)

			bias_key = f"bias_q_{i + 1}"
			# self.biases_q.append(tf.get_variable(name=bias_key, shape=[d_out], 
			#                      initializer=tf.truncatd_normal_initializer( stddev=0.001, seed=self.random_seed)))
			bias = torch.empty(d_out, dtype=torch.float32, requires_grad=True, device=device)
			bias = truncated_normal(bias)
			self.biases_q.append(bias)
			# print(bias)

		# self.items = tf.get_variable(name="items", shape=[num_items, dfac],
		#                              initializer=tf.contrib.layers.xavier_initializer(seed=self.random_seed))
		self.items = torch.empty(num_items, dfac, dtype=torch.float32, requires_grad=True, device=device)
		nn.init.xavier_uniform_(self.items)
		# print(self.items)
		
		# self.cores = tf.get_variable(name="cores", shape=[kfac, dfac], 
		#                              initializer=tf.contrib.layers.xavier_initializer(seed=self.random_seed))
		self.cores = torch.empty(kfac, dfac, dtype=torch.float32, requires_grad=True, device=device)
		nn.init.xavier_uniform_(self.cores)
		# print(self.cores)

		######### Input data
		# self.input_ph       = tf.placeholder(dtype=tf.float32, shape=[None, num_items])
		# self.keep_prob_ph   = tf.placeholder_with_default(1., shape=None)
		# self.is_training_ph = tf.placeholder_with_default(0., shape=None)
		# self.anneal_ph      = tf.placeholder_with_default(1., shape=None)
		self.input_ph       = None
		self.keep_prob_ph   = torch.tensor([1.], dtype=torch.float32, device=device)
		self.is_training_ph = torch.tensor([0.], dtype=torch.float32, device=device)
		self.anneal_ph      = torch.tensor([1.], dtype=torch.float32, device=device)
		
	def forward(self, x, save_emb=False):
		# Seperating various inputs
		self.input_ph       = x["input_ph"]
		self.keep_prob_ph   = x["keep_prob_ph"]
		self.is_training_ph = x["is_training_ph"]
		self.anneal_ph      = x["anneal_ph"]

		if save_emb:
			facets_list = self.forward_pass(save_emb=True)
			return facets_list, self.items, self.cores

		logits, recon_loss, kl = self.forward_pass(save_emb=False)
		return logits, recon_loss, kl

	def forward_pass(self, save_emb):
		# cores      = tf.nn.l2_normalize(self.cores, axis=1)
		# items      = tf.nn.l2_normalize(self.items, axis=1)
		# cat_logits = tf.matmul(items, cores, transpose_b=True) / pp.tau

		cores      =  F.normalize(self.cores, dim=1, p=2)
		items      =  F.normalize(self.items, dim=1, p=2)
		cat_logits =  torch.div(torch.mm(items, torch.t(cores)), pp.tau)

		m = nn.Softmax(dim=1)
		if pp.nogb:
			# cat = tf.nn.softmax(cat_logits, axis=1)
			cat = m(cat_logits)
		else:
		    ### Graumbel Softmax smapling smooth for Category
		    # cat_dist   = RelaxedOneHotcatgorical(1, cat_logits)
		    # cat_sample = cat_dist.sample()
		    # cat_mode   = tf.nn.softmax(cat_logits, axis=1)
		    # cat        = (self.is_training_ph * cat_sample + (1 - self.is_training_ph) * cat_mode)
		    cat_dist   = RelaxedOneHotCategorical(torch.tensor([1.0], device=device), cat_logits)
		    cat_sample = cat_dist.sample()
		    cat_mode   = m(cat_logits)
		    cat        = (self.is_training_ph * cat_sample + (1 - self.is_training_ph) * cat_mode)

		##### Loop on all K-factors   ########################################################################
		z_list = []
		probs, kl = None, None
		for k in range(pp.kfac):
			# cat_k = tf.reshape(cat[:, k], (1, -1))
			cat_k = cat[:,k].view(1, -1)

			# # q-network  :  Encode @@@@@@@@@@@@@@@@@@@@@@@@@@@@
			# x_k 			  = self.input_ph * cat_k
			# mu_k, std_k, kl_k = self.q_graph_k(x_k)
			# epsilon           = tf.random_normal(tf.shape(std_k))
			# z_k               = mu_k + self.is_training_ph * epsilon * std_k   ###Generate Laten factors
			# kl                = (kl_k if (kl is None) else (kl + kl_k))
			# if save_emb: z_list.append(z_k)

			x_k 			  = self.input_ph * cat_k
			mu_k, std_k, kl_k = self.q_graph_k(x_k)
			epsilon           = torch.randn(std_k.shape, device=device)
			z_k               = mu_k + self.is_training_ph * epsilon * std_k   ###Generate Laten factors
			kl                = (kl_k if (kl is None) else (kl + kl_k))
			if save_emb: z_list.append(z_k)

			# # p-network ; Reconstruct  ##########################
			# z_k      = tf.nn.l2_normalize(z_k, dim=1)
			# logits_k = tf.matmul(z_k, items, transpose_b=True) / pp.tau
			# probs_k  = tf.exp(logits_k)
			# probs_k  = probs_k * cat_k
			# probs    = (probs_k if (probs is None) else (probs + probs_k))

			# p-network ; Reconstruct  ##########################
			z_k      = F.normalize(z_k, dim=1, p=2)
			logits_k = torch.div(torch.mm(z_k, torch.t(items)), pp.tau)
			probs_k  = torch.exp(logits_k)
			probs_k  = probs_k * cat_k
			probs    = (probs_k if (probs is None) else (probs + probs_k))

		# logits     = tf.log(probs)
		# logits     = tf.nn.log_softmax(logits)
		# recon_loss = tf.reduce_mean(tf.reduce_sum( -logits * self.input_ph, axis=-1))
		# if save_emb: return tf.train.Saver(), z_list
		# return tf.train.Saver(), logits, recon_loss, kl
		applyLogSoftmax = nn.LogSoftmax()
		logits          = torch.log(probs)
		logits          = applyLogSoftmax(logits)
		recon_loss      = torch.mean(torch.sum( -logits * self.input_ph, axis=-1))

		if save_emb: return z_list
		return logits, recon_loss, kl     	

	def q_graph_k(self, x):
		### Macro Regularization
		# mu_q, std_q, kl = None, None, None
		# h = tf.nn.l2_normalize(x, 1)
		# h = tf.nn.dropout(h, self.keep_prob_ph)
		mu_q, std_q, kl = None, None, None
		h = F.normalize(x, dim=1, p=2)
		dropout_layer_ = nn.Dropout(p=self.keep_prob_ph, inplace=True)
		dropout_layer_(h)

		for i, (w, b) in enumerate(zip(self.weights_q, self.biases_q)):
			# h = tf.matmul(h, w, a_is_sparse=(i == 0)) + b
			# if i != len(self.weights_q) - 1:
			#     h = tf.nn.tanh(h)
			# else:
			#     mu_q              = h[:, :self.q_dims[-1]]
			#     mu_q              = tf.nn.l2_normalize(mu_q, dim=1)
			#     lnvarq_sub_lnvar0 = -h[:, self.q_dims[-1]:]
			#     std0              = pp.std
			#     std_q             = tf.exp(0.5 * lnvarq_sub_lnvar0) * std0
			#     # Trick: KL is constant w.r.t. to mu_q after we normalize mu_q.
			#     kl = tf.reduce_mean(tf.reduce_sum(0.5 * (-lnvarq_sub_lnvar0 + tf.exp(lnvarq_sub_lnvar0) - 1.), axis=1))

			# print(h.shape)
			h = torch.add(torch.mm(h, w), b)
			if i != len(self.weights_q) - 1:
			    non_linearity = nn.Tanh()
			    h = non_linearity(h)
			else:
				mu_q              = h[:, :self.q_dims[-1]]
				mu_q			  = F.normalize(mu_q, dim=1, p=2)
				lnvarq_sub_lnvar0 = -1 * h[:, self.q_dims[-1]:]
				std0              = pp.std
				std_q             = torch.exp(0.5 * lnvarq_sub_lnvar0) * std0

				#     # Trick: KL is constant w.r.t. to mu_q after we normalize mu_q.
				kl = torch.mean(torch.sum((0.5 * (-1 * lnvarq_sub_lnvar0 + torch.exp(lnvarq_sub_lnvar0) - 1.)), dim=1))

		return mu_q, std_q, kl

	# def calculate_loss(self, loss):
	# 	recon_loss = loss["recon_loss"]
	# 	kl = loss["kl"]
	# 	# reg_var  = apply_regularization(l2_regularizer(self.lam), self.weights_q + [self.items, self.cores])
	# 	# neg_elbo = recon_loss + self.anneal_ph * kl + 2. * reg_var   # tensorflow l2 regularization multiply 0.5 to the l2 norm,         # multiply 2 so that it is back in the same scale
	# 	# train_op = tf.train.AdamOptimizer(self.lr).minimize(neg_elbo)

	# 	# reg_var  = apply_regularization(l2_regularizer(self.lam), self.weights_q + [self.items, self.cores])

	# 	l2_reg = None
	# 	# weights = self.weights_q + [self.items, self.cores]
	# 	for W in self.weights_q + [self.items, self.cores]:
	# 		if l2_reg is None:
	# 			l2_reg = W.norm(2)
	# 		else:`
	# 			l2_reg = l2_reg + W.norm(2)

	# 	neg_elbo = recon_loss + (self.anneal_ph * kl) + (2. * l2_reg * self.lam)
	# 	return neg_elbo

# Function to generate truncated normal dist
def truncated_normal(tensor, mean=0, std=1):
    size = tensor.shape
    tmp = tensor.new_empty(size + (4,)).normal_()
    valid = (tmp < 2) & (tmp > -2)
    ind = valid.max(-1, keepdim=True)[1]
    tensor.data.copy_(tmp.gather(-1, ind).squeeze(-1))
    tensor.data.mul_(std).add_(mean)
    return tensor

def np_normalize(x, axis=-1, eps=1e-12):
    norm = np.linalg.norm(x, axis=axis, keepdims=True)
    norm[norm < eps] = eps
    return x / norm

def matchness_of_cores_and_cat(cores, items, cat_items):
    k = cat_items.shape[1]
    cat = np.argmax(cat_items, axis=1)
    cat_centers = []
    for j in range(k):
        cat_centers.append(items[cat == j].sum(axis=0, keepdims=True))
    cat_centers = np.concatenate(cat_centers, axis=0)
    cat_centers = np_normalize(cat_centers)

    core_vs_cat = cores.dot(cat_centers.T)
    print('core_vs_cat =\n', core_vs_cat, file=sys.stderr)

    best_cat_for_core = np.argmax(core_vs_cat, axis=1)
    print('best_cat_for_core = ', best_cat_for_core, file=sys.stderr)

    best_core_for_cat = np.argmax(core_vs_cat, axis=0)
    print('best_core_for_cat = ', best_core_for_cat, file=sys.stderr)

    interpretability = 0
    if len(set(best_core_for_cat)) == k:  interpretability += 1
    if len(set(best_cat_for_core)) == k:  interpretability += 1

    if interpretability >= 2:
        inconsistent = False
        for j in range(k):
            if best_core_for_cat[best_cat_for_core[j]] != j:
                inconsistent = True
                break
        if not inconsistent:  interpretability += 1
    return interpretability, best_cat_for_core


def visualize_macro(users, items, cores, train_data):
    palette = np.asarray(
        [[238, 27., 39., 80],  # _0. Red
         [59., 175, 81., 80],  # _1. Green
         [255, 127, 38., 80],  # _2. Orange
         [255, 129, 190, 80],  # _3. Pink
         [153, 153, 153, 80],  # _4. Gray
         [156, 78., 161, 80],  # _5. Purple
         [35., 126, 181, 80]],  # 6. Blue
        dtype=np.float32) / 255.0

    n, m = users.shape[0], items.shape[0]
    k, d = cores.shape
    users = users.reshape(n, k, d)
    assert items.shape[1] == d
    del n

    users = np_normalize(users)
    items = np_normalize(items)
    cores = np_normalize(cores)

    cat_items = load_item_cat(pp.data, m)
    interpretable, core2cat = matchness_of_cores_and_cat( cores, items, cat_items)
    print('macro interpretable = %d [seed = %d]' % (interpretable, pp.seed), file=sys.stderr)
    if interpretable < 3:
        print('Some prototypes do not align well with catgories. Maybe try another random seed? :)', file=sys.stderr)
        return interpretable

    cat_items = cat_items[:, core2cat]  # align catgories with prototypes
    gold_items = np.argmax(cat_items, axis=1)
    pred_items = np.argmax(items.dot(cores.T), axis=1)

    ufacs, gold_ufacs = [], []  # user facets
    is_in_ufacs = set()
    for u, i in zip(*train_data.nonzero()):
        c = gold_items[i]
        if (u, c) not in is_in_ufacs:
            is_in_ufacs.add((u, c))
            ufacs.append(users[u, c].reshape(1, d))
            gold_ufacs.append(c)
    del is_in_ufacs
    ufacs = np.concatenate(ufacs, axis=0)
    gold_ufacs = np.asarray(gold_ufacs, dtype=np.int64)
    n = ufacs.shape[0]
    assert ufacs.shape == (n, d)
    assert gold_ufacs.shape == (n,)

    vis_dir = osj(LOG_DIR, 'vis')
    if os.path.exists(vis_dir): shutil.rmtree(vis_dir)

    def plot(title, xy, color, marksz=2.):
        fig, ax = plt.subplots()
        ax.scatter(x=xy[:, 0], y=xy[:, 1], s=marksz, c=color)
        plt.savefig('%s.png' % title.replace('/', '-'), format='png', dpi=160)

    nodes = np.concatenate((items, ufacs), axis=0)
    assert nodes.shape == (m + n, d)
    gold_nodes = np.concatenate((gold_items, gold_ufacs), axis=0)
    assert gold_nodes.shape == (m + n,)
    pred_nodes = np.argmax(nodes.dot(cores.T), axis=1)

    col_pred = palette[pred_nodes]
    col_gold = palette[gold_nodes]

    tsne2d_sav = osj(LOG_DIR, 'tsne2d-nodes.npy')
    if os.path.isfile(tsne2d_sav):x_2d = np.load(tsne2d_sav)
    else:
        x_2d = nodes
        if d > k: x_2d = sklearn.decomposition.PCA(n_components=k).fit_transform(x_2d)

        print('Running tSNE...', file=sys.stderr)
        x_2d = sklearn.manifold.TSNE(n_components=2, perplexity=30).fit_transform(x_2d)
        print('Finished tSNE...', file=sys.stderr)
        np.save(tsne2d_sav, x_2d)
    plot('tsne2d-nodes/pred', x_2d, col_pred)
    plot('tsne2d-nodes/gold', x_2d, col_gold)
    plot('tsne2d-items/pred', x_2d[:m], col_pred[:m])
    plot('tsne2d-items/gold', x_2d[:m], col_gold[:m])
    plot('tsne2d-users/pred', x_2d[m:], col_pred[m:])
    plot('tsne2d-users/gold', x_2d[m:], col_gold[m:])

    return interpretable

def main():
	(_, train_data, vad_data_tr, vad_data_te, tst_data_tr, tst_data_te) = load_data(pp.data)

	set_rng_seed(pp.seed)
	n                  = train_data.shape[0]
	n_items            = train_data.shape[1]; print(n_items)
	idxlist            = list(range(n))
	n_vad              = vad_data_tr.shape[0]
	idxlist_vad        = list(range(n_vad))
	num_batches        = int(np.ceil(float(n) / pp.batch))
	total_anneal_steps = 5 * num_batches

	vae = MyVae(n_items)
	if pp.gpu:
		vae.cuda()
	# print(vae.weights_q, vae.items, vae.cores)

	optimizer = optim.Adam(vae.weights_q + vae.biases_q + [vae.items, vae.cores], lr=pp.lr, weight_decay=pp.rg)

	# saver, logits_var, train_op_var, merged_var = vae.build_graph()  # Build the graph

	# if os.path.exists(LOG_DIR):  shutil.rmtree(LOG_DIR)
	# summary_writer = tf.summary.FileWriter(LOG_DIR, graph=tf.get_default_graph())
	# if not os.path.isdir(LOG_DIR):  os.makedirs(LOG_DIR)

	best_ndcg    = -np.inf
	update_count = 0.0
	########### Loop  ######################################
	for epoch in range(pp.epoch):
		np.random.shuffle(idxlist)
		for bnum, st_idx in enumerate(range(0, n, pp.batch)):
			end_idx = min(st_idx + pp.batch, n)
			x       = train_data[idxlist[st_idx:end_idx]]
			if sparse.isspmatrix(x): x = x.toarray()
			x = x.astype('float32')

			anneal = min(pp.beta, 1. * update_count / total_anneal_steps) if total_anneal_steps > 0 else pp.beta
			# print(x, len(x), len(x[0]), pp.keep, anneal)

			input_ph       = torch.tensor(x, requires_grad=False, dtype=torch.float32, device=device)
			keep_prob_ph   = torch.tensor(pp.keep, requires_grad=False, dtype=torch.float32, device=device)
			anneal_ph      = torch.tensor(anneal, requires_grad=False, dtype=torch.float32, device=device)
			is_training_ph = torch.tensor(1, requires_grad=False, dtype=torch.float32, device=device)

			x = dict()
			x["input_ph"]       = input_ph
			x["keep_prob_ph"]   = keep_prob_ph
			x["is_training_ph"] = is_training_ph
			x["anneal_ph"]	    = anneal_ph

			logits, recon_loss, kl = vae(x)

			# loss = dict()
			# loss["recon_loss"] = recon_loss
			# loss["kl"]		   = kl

			# neg_elbo = vae.calculate_loss(loss)
			# print("neg_elbo", neg_elbo)

			neg_elbo = recon_loss + (anneal_ph * kl)
			# print("logits:", logits, neg_elbo, recon_loss, kl)
			print("neg_elbo:", neg_elbo, recon_loss, kl)

			optimizer.zero_grad()
			neg_elbo.backward(); # print("Gradient", vae.weights_q[-1].grad)
			optimizer.step()

		# break

		#### Evluation metrics ###########################################################################
		ndcg_dist = []
		for bnum, st_idx in enumerate(range(0, n_vad, batch_size_vad)):
		    end_idx = min(st_idx + batch_size_vad, n_vad)
		    x       = vad_data_tr[idxlist_vad[st_idx:end_idx]]
		    if sparse.isspmatrix(x): x = x.toarray()
		    x = x.astype('float32')

		    input_ph       = torch.tensor(x, requires_grad=False, dtype=torch.float32, device=device)
			keep_prob_ph   = torch.tensor(1, requires_grad=False, dtype=torch.float32, device=device)
			is_training_ph = torch.tensor(0, requires_grad=False, dtype=torch.float32, device=device)
			anneal_ph      = torch.tensor(1, requires_grad=False, dtype=torch.float32, device=device)
			
		    # pred_val = sess.run(logits_var, feed_dict={vae.input_ph: x})
		    logits, _, _ = vae(x)
		    pred_val = logits.cpu().data.numpy()

		    # exclude examples from training and validation (if any)
		    pred_val[x.nonzero()] = -np.inf
		    ndcg_dist.append(ndcg_binary_at_k_batch(pred_val, vad_data_te[idxlist_vad[st_idx:end_idx]]))


		ndcg_dist = np.concatenate(ndcg_dist)
		ndcg      = ndcg_dist.mean()
		print("ndcg: ", ndcg)
		# if ndcg > best_ndcg:
		#     saver.save(sess, '{}/chkpt'.format(LOG_DIR))
		#     best_ndcg = ndcg

		# merged_valid_val = sess.run(merged_valid, feed_dict={ndcg_var: ndcg, ndcg_best_var: best_ndcg})
		# summary_writer.add_summary(merged_valid_val, epoch)

	# print(vae.weights_q, vae.items, vae.cores)

	# n_items = 20591

	# import pickle
	# f = open("x.pickle", "rb")
	# input_ph ,keep_prob_ph, anneal_ph, is_training_ph = pickle.load(f)
	# f.close()

	# # print(input_ph ,keep_prob_ph, anneal_ph, is_training_ph)
	# # print(type(input_ph) ,type(keep_prob_ph), type(anneal_ph), type(is_training_ph)

	# vae = MyVae(n_items)
	# print(vae.weights_q, vae.items, vae.cores)

	# # exit()
	# optimizer = optim.Adam(vae.weights_q + [vae.items, vae.cores], lr=pp.lr)

	# input_ph       = torch.tensor(input_ph, requires_grad=False, dtype=torch.float32)
	# keep_prob_ph   = torch.tensor(keep_prob_ph, requires_grad=False, dtype=torch.float32)
	# anneal_ph      = torch.tensor(anneal_ph, requires_grad=False, dtype=torch.float32)
	# is_training_ph = torch.tensor(is_training_ph, requires_grad=False, dtype=torch.float32)
	
	# x = dict()
	# x["input_ph"]       = input_ph
	# x["keep_prob_ph"]   = keep_prob_ph
	# x["is_training_ph"] = is_training_ph
	# x["anneal_ph"]	    = anneal_ph

	# optimizer.zero_grad()
	# logits, recon_loss, kl = vae(x)
	# print("logits:", logits, recon_loss, kl)

	# loss = dict()
	# loss["recon_loss"] = recon_loss
	# loss["kl"]		   = kl

	# neg_elbo = vae.calculate_loss(loss)
	# print("neg_elbo", neg_elbo)

	# neg_elbo.backward()
	# optimizer.step()

	# print(vae.weights_q, vae.items, vae.cores)


if __name__ == '__main__':
	main()
