# Исследование вариационных автоэнкодеров [в процессе]

![pic](https://github.com/Arzamazov/VAE_research/blob/master/experiments/MNIST/betaVAE%20similar%20sampling/beta0.01_stddev0.3.png?raw=true)

1. Основная тетрадка с результатами экспериментов и выводами: https://github.com/Arzamazov/VAE_research/blob/master/Experiments_with_disentangled_representation_in_VAE.ipynb

2. Тетрадка с процессом исследования базовых моделей на датасете MNIST
https://github.com/Arzamazov/VAE_research/blob/master/VAEs_on_MNIST_in_process.ipynb

3. Тетрадка с процессом исследования распутывания представлений на датасете dSprites
https://github.com/Arzamazov/VAE_research/blob/master/betaVAE_on_dSprites_process.ipynb
